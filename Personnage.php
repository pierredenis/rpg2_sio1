<?php

abstract class Personnage{

    protected $pv;
    protected $pvMax;
    protected $nom;
    protected $vivant;
    protected $force;

    public function __construct($pvMax,$nom){
        $this->pv = $pvMax;
        $this->pvMax = $pvMax;
        $this->nom = $nom;
        $this->vivant = true;
        $this->force = rand(1,5);
    }

    public function getPv(){
        return $this->pv;
    }
    public function getPvMax(){
        return $this->pvMax;
    }
    public function getNom(){
        return $this->nom;
    }

    public function soin($pv){
        if($this->vivant){
            if($this->pv + $pv > $this->pvMax){
                $this->pv = $this->pvMax;
            }else{
                $this->pv = $this->pv + $pv;
            }
        }

    }

    public function subirDegats($pv){
        if($this->pv - $pv <= 0){
            $this->pv = 0;
            $this->vivant = false;
        }else{
            $this->pv = $this->pv - $pv;
        }
    }

    public function reanimation(){
        $this->vivant = true;
        $this->pv = $this->pvMax * 0.05;
    }

    public function attaquer(Personnage $perso){
        $perso->subirDegats($this->force);
    }

    static public function destroy(Array $persos){
        foreach($persos as $perso){
            $perso->pv = 0;
            $perso->vivant = false;
        }
    }

}

?>