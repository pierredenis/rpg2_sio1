<?php

require_once 'Personnage.php';

class Guerrier extends Personnage {

    public function __construct($pvMax, $nom)
    {
        parent::__construct($pvMax, $nom);
        $this->force = rand(10,20);
    }

}

?>