<?php

require_once 'Mage.php';

final class MageBlanc extends Mage{

    public function __construct($pvMax, $nom, $mana)
    {
        parent::__construct($pvMax, $nom, $mana);
    }

    public function reanimation()
    {
        $this->vivant = true;
        $this->pv = $this->pvMax * 0.15;
    }

    public function soin($pv){
        $pv = $pv *1.15;
        if($this->vivant){
            if($this->pv + $pv > $this->pvMax){
                $this->pv = $this->pvMax;
            }else{
                $this->pv = $this->pv + $pv;
            }
        }

    }

}

?>